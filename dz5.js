const gameField = ['x','x', 'o', 'x', 'o', 'x', 'o', 'x', 'o'];

console.log(gameField[0], gameField[1], gameField[2]) //#для удобства выведена матрица 1 ряда
console.log(gameField[3], gameField[4], gameField[5]) //#для удобства выведена матрица 2 ряда
console.log(gameField[6], gameField[7], gameField[8]) //#для удобства выведена матрица 3 ряда

const lineGorX1 = gameField[0] === gameField[1] && gameField[0] === gameField[2] && gameField[0] === 'x';
const lineGorX2 = gameField[3] === gameField[4] && gameField[3] === gameField[5] && gameField[3] === 'x';
const lineGorX3 = gameField[6] === gameField[7] && gameField[6] === gameField[8] && gameField[6] === 'x';

const lineGorO1 = gameField[0] === gameField[1] && gameField[0] === gameField[2] && gameField[0] === 'o';
const lineGorO2 = gameField[3] === gameField[4] && gameField[3] === gameField[5] && gameField[3] === 'o';
const lineGorO3 = gameField[6] === gameField[7] && gameField[6] === gameField[8] && gameField[6] === 'o';

const lineVerX1 = gameField[0] === gameField[3] && gameField[0] === gameField[6] && gameField[0] === 'x';
const lineVerX2 = gameField[1] === gameField[4] && gameField[1] === gameField[7] && gameField[1] === 'x';
const lineVerX3 = gameField[2] === gameField[5] && gameField[2] === gameField[8] && gameField[2] === 'x';

const lineVerO1 = gameField[0] === gameField[3] && gameField[0] === gameField[6] && gameField[0] === 'o';
const lineVerO2 = gameField[1] === gameField[4] && gameField[1] === gameField[7] && gameField[1] === 'o';
const lineVerO3 = gameField[2] === gameField[5] && gameField[2] === gameField[8] && gameField[2] === 'o';

const lineDiaX1 = gameField[0] === gameField[4] && gameField[0] === gameField[8] && gameField[0] === 'x';
const lineDiaX2 = gameField[2] === gameField[4] && gameField[2] === gameField[6] && gameField[2] === 'x';

const lineDiaO1 = gameField[0] === gameField[4] && gameField[0] === gameField[8] && gameField[0] === 'o';
const lineDiaO2 = gameField[2] === gameField[4] && gameField[2] === gameField[6] && gameField[2] === 'o';

if ( lineGorX1 === true || lineGorX2 === true || lineGorX3 === true) {
    console.log('Крестики победили');
}
else if (lineGorO1 === true || lineGorO2 === true || lineGorO3 === true){
    console.log('Нолики победили')
}
else if (lineVerX1 === true || lineVerX2 === true || lineVerX3 === true) {
    console.log('Крестики победили')
}
else if (lineVerO1 === true || lineVerO2 === true || lineVerO3 === true) {
    console.log('Нолики победили')
}
else if (lineDiaX1 === true || lineDiaX2 === true) {
    console.log('Крестики победили')
}
else if (lineDiaO1 === true || lineDiaO2 === true) {
    console.log('Нолики победили')
}
else (console.log('Ничья'));
